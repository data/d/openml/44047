# OpenML dataset: Brazilian_houses

https://www.openml.org/d/44047

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset used in the tabular data benchmark https://github.com/LeoGrin/tabular-benchmark,  
                                  transformed in the same way. This dataset belongs to the "regression on categorical and
                                  numerical features" benchmark. Original description: 
 
**Author**: Kaggle  
**Source**: [original](https://www.kaggle.com/rubenssjr/brasilian-houses-to-rent) - 20-03-2020  
**Please cite**:   

This dataset contains 10962 houses to rent with 13 diferent features.

**Outliers **
Some values in the dataset can be considered as outliers for further analyses. Bear in mind that the Web Crawler was used only to get the data, so it's possible that errors in the original data exist.

**Changes in data between versions of dataset **
Since the WebCrawler was ran in different days for each version of dataset, there may be differences like added or deleted houses (as well as added cities).

Notes: 

1) This dataset corresponds to the 2nd version of the original dataset ("houses_to_rent_v2.csv").

2) The value '-' in the attribute floor was replaced by '0' as the data contributor stated that this refers to houses with just one floor (see https://www.kaggle.com/rubenssjr/brasilian-houses-to-rent/discussion).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44047) of an [OpenML dataset](https://www.openml.org/d/44047). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44047/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44047/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44047/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

